from django.contrib import admin

# Register your models here.
from products.models import Product, Attributes, Price

admin.site.register(Product)
admin.site.register(Attributes)
admin.site.register(Price)