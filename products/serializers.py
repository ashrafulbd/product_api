from drf_writable_nested import WritableNestedModelSerializer

from products.models import Attributes, Product, Price


class AttributesSerializer(WritableNestedModelSerializer):
    class Meta:
        model = Attributes
        fields = '__all__'



class PriceSerializer(WritableNestedModelSerializer):
    class Meta:
        model = Price
        fields = '__all__'


class ProductSerializer(WritableNestedModelSerializer):
    attributes = AttributesSerializer(many=True)
    price = PriceSerializer(many=True)

    class Meta:
        model = Product

        fields = ['pk', 'product_name', "attributes", 'price']
        # extra_fields = ['attributes']


    #
    # def update(self, instance, validated_data):
    #
    #
    #     print(validated_data['attributes'][0])       # form data
    #     # print(self.data['attributes'][0]['key'])
    #
    #     validated_data_key_list = validated_data['attributes'][0]
    #     instance_key_list = self.data['attributes'][0]['key']
    #
    #     for i in validated_data_key_list:
    #         for j in instance_key_list:
    #
    #             print(j)
    #
    #     # if
    #
    #
    #
    #
    #     # Update the Foo instance
    #     # instance.title = validated_data['title']
    #     # instance.save()
    #     return instance
