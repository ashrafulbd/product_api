from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from products.models import Product, Attributes, Price
from products.serializers import ProductSerializer, AttributesSerializer, PriceSerializer
from rest_framework import viewsets, status


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    # lookup_field = 'pk'




class AttributesView(APIView):
    def get(self, request, pk):

        # print(self.kwargs['pk'])
        # articles = Attributes.objects.all()
        product = Product.objects.get(pk=pk).attributes
        serializer = AttributesSerializer(product, many=True)
        return Response({"attributes": serializer.data})



    def post(self, request, pk):

        try:
            new_attribute = Attributes(key=request.data['key'], value=request.data['value'])
            new_attribute.save()

            product_update = Product.objects.get(pk=pk)
            product_update.attributes.add(new_attribute)
            articles = Product.objects.get(pk=pk).attributes
            serializer = AttributesSerializer(articles, many=True)
            return Response({"attributes": serializer.data}, status=status.HTTP_201_CREATED)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # serializer = SnippetSerializer(data=request.data)
        # if serializer.is_valid():
        #     serializer.save()
        #     return Response(serializer.data, status=status.HTTP_201_CREATED)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
















class pricesView(APIView):
    def get(self, request, pk):

        # print(self.kwargs['pk'])
        # articles = Attributes.objects.all()
        product = Product.objects.get(pk=pk).price
        serializer = PriceSerializer(product, many=True)
        return Response({"prices": serializer.data})



    def post(self, request, pk):

        try:
            new_price = Price(from_date=request.data['from_date'], to_date=request.data['to_date'], price=request.data['price'])
            new_price.save()

            product_update = Product.objects.get(pk=pk)
            product_update.price.add(new_price)
            prices = Product.objects.get(pk=pk).price
            serializer = PriceSerializer(prices, many=True)
            return Response({"prices": serializer.data}, status=status.HTTP_201_CREATED)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # serializer = SnippetSerializer(data=request.data)
        # if serializer.is_valid():
        #     serializer.save()
        #     return Response(serializer.data, status=status.HTTP_201_CREATED)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)