from django.db import models

# Create your models here.
from django.db.models import Max


class Attributes(models.Model):

    key = models.CharField(max_length=10, blank=False, null=False)
    value = models.CharField(max_length=20, blank=False, null=False)

    def __str__(self):
        return self.key


class Price(models.Model):

    from_date = models.DateField(blank=False, null=False)
    to_date = models.DateField(blank=False, null=False)
    price = models.CharField(max_length=8, blank=False, null=False)

    def __str__(self):
        return str(self.pk)


def get_product_code():
    try:
        last_id = Product.objects.aggregate(Max('pk'))
        return int(last_id['pk__max']) + 1
    except:
        return 1000


class Product(models.Model):

    product_name = models.CharField(max_length=50, blank=False, null=False)
    product_code = models.CharField(primary_key=True, max_length=6, default=get_product_code, editable=False)

    attributes = models.ManyToManyField(Attributes)
    price = models.ManyToManyField(Price)

    def __str__(self):
        return self.product_name