from datetime import date

from django.test import TestCase

from products.models import Attributes, Price, Product


class AttributesModelTest(TestCase):
    @classmethod
    def test_setUpTestData(cls):
        Attributes.objects.create(key='color', value='red')





class PriceModelTest(TestCase):
    @classmethod
    def test_setUpTestData(cls):
        Price.objects.create(from_date=date.today(), to_date='2019-12-22', price='200')



class ProductModelTest(TestCase):
    @classmethod
    def test_setUpTestData(cls):
        attributes = [Attributes.objects.create(key='color', value='red'), Attributes.objects.create(key='size', value='XL')]
        price = [Price.objects.create(from_date=date.today(), to_date='2019-12-22', price='200'), Price.objects.create(from_date='2019-12-30', to_date='2020-12-22', price='315')]

        p = Product.objects.create(product_name='new product',)

        for i in attributes:
            p.attributes.add(i)
        for i in price:
            p.price.add(i)


