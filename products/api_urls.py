from rest_framework.routers import SimpleRouter

from products.views import ProductViewSet, AttributesView, pricesView
from django.urls import path, include




routerproduct = SimpleRouter()
routerproduct.register('', ProductViewSet)


urlpatterns = [

    path('products/', include(routerproduct.urls)),
    # path('att/', AttributesView.as_view()),
    path('products/<int:pk>/attributes/', AttributesView.as_view()),
    path('products/<int:pk>/prices/', pricesView.as_view()),

]