# Products App

## Installation: 

* Install all requirements from the req.txt
* Do makemigration and migrate stuffs
* Crate superuser for admin role
* Run the Django app


## API End Points: 

* api/products/

    [
        {
            "pk": "1001",
            "product_name": "pro1",
            "attributes": [
                {
                    "id": 1,
                    "key": "at1",
                    "value": "value1"
                },
                {
                    "id": 2,
                    "key": "color",
                    "value": "red"
                }
            ],
            "price": [
                {
                    "id": 1,
                    "from_date": "2019-12-04",
                    "to_date": "2019-12-04",
                    "price": "5000"
                },
                {
                    "id": 2,
                    "from_date": "2019-12-04",
                    "to_date": "2019-12-04",
                    "price": "600"
                },
                {
                    "id": 3,
                    "from_date": "2019-12-04",
                    "to_date": "2019-12-04",
                    "price": "100"
                }
            ]
        },
        {
            "pk": "1002",
            "product_name": "product1",
            "attributes": [
                {
                    "id": 3,
                    "key": "size",
                    "value": "XL"
                },
                {
                    "id": 4,
                    "key": "color",
                    "value": "red"
                }
            ],
            "price": [
                {
                    "id": 1,
                    "from_date": "2019-12-04",
                    "to_date": "2019-12-04",
                    "price": "5000"
                },
                {
                    "id": 2,
                    "from_date": "2019-12-04",
                    "to_date": "2019-12-04",
                    "price": "600"
                },
                {
                    "id": 3,
                    "from_date": "2019-12-04",
                    "to_date": "2019-12-04",
                    "price": "100"
                }
            ]
        }
    ]



* api/products/<product_code>/

    {
        "pk": "1002",
        "product_name": "product1",
        "attributes": [
            {
                "id": 3,
                "key": "size",
                "value": "XL"
            },
            {
                "id": 4,
                "key": "color",
                "value": "red"
            }
        ],
        "price": [
            {
                "id": 1,
                "from_date": "2019-12-04",
                "to_date": "2019-12-04",
                "price": "5000"
            },
            {
                "id": 2,
                "from_date": "2019-12-04",
                "to_date": "2019-12-04",
                "price": "600"
            },
            {
                "id": 3,
                "from_date": "2019-12-04",
                "to_date": "2019-12-04",
                "price": "100"
            }
        ]
    }


* api/products/<product_code>/attributes/

    {
        "attributes": [
            {
                "id": 3,
                "key": "size",
                "value": "XL"
            },
            {
                "id": 4,
                "key": "color",
                "value": "red"
            }
        ]
    }



* api/products/<product_code>/prices/

    {
        "prices": [
            {
                "id": 1,
                "from_date": "2019-12-04",
                "to_date": "2019-12-04",
                "price": "5000"
            },
            {
                "id": 2,
                "from_date": "2019-12-04",
                "to_date": "2019-12-04",
                "price": "600"
            },
            {
                "id": 3,
                "from_date": "2019-12-04",
                "to_date": "2019-12-04",
                "price": "100"
            }
        ]
    }